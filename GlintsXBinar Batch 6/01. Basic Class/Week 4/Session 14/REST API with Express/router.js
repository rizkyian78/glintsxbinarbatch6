// Route Level Middleware
const router = require('express').Router();

console.log(router);

// Import Controller
const { getProducts, postProducts } = require('./Controller/product');
const { getUser, login } = require('./Controller/user')

/* Product API Collection */
router.get('/products', getProducts);
router.post('/products', postProducts);

/* User API Collection */
router.get('/users', getUser);
router.post('/login', login);

module.exports = router;