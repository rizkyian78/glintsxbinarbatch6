const express = require('express');
const app = express();

const user = require('./db/user.json');
const product = require('./db/product.json')

// MiddleWare to parse JSON
console.log(app.use(express.json()));

// GET
app.get('/', (req, res, next) => {
    res.send("Hello World");
})
app.get('/products', (req, res, next) => {
    res.json(product)
    console.log("End Point Product");
});
app.get('/user', (req, res) => {
    let entity = {...user};
    delete entity.password
    res.status(200).json(entity)
  })

app.post('/login', (req, res) => {
    if(req.body.email !== user.email) {
        return res.status(401).json({
            status: false,
            message: "Email doesn't exist"
        })
    }
    if (user.password !== req.body.password)
    return res.status(401).json({
      status: false,
      message: "Wrong password!"
    })
    res.status(200).json({
        status: true,
        message: "Succesfully logged in"
      })
  })
app.use((req, res, next) => {
    res.send("Blok")
})

app.listen(3000, () => {
    console.log("Halo Babi");
})