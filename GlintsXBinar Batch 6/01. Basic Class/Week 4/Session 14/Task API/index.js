const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = 3000;

const router = require('./Router');

app.use(express.json());
app.use(cors())
app.use(bodyParser.json())

app.use('/', router);

app.get('/info', (req, res) => {
    res.status(200).json({
        status: true,
        data: [
            "POST /calculate/circleArea",
            "POST /calculate/squareArea",
            "POST /calculate/cubeVolume",
            "POST /calculate/tubeVolume",
            "POST /calculate/triangleArea",
        ]
    })
})
app.use((req,res) => {
    res.send(`<h1>Tidak ada url dengan nama ${req.url}</h1>`)
})

app.listen(port, () => console.log(`Server is Running ${port}`))