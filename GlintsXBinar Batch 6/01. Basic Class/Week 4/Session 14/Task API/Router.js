const express = require('express');
const router = express.Router();

const controller = require('./Controller/Controller');

router.post('/calculate/circleArea', controller.circleArea);
router.post('/calculate/squareArea', controller.squareArea);
router.post('/calculate/cubeVolume', controller.cubeVolume);
router.post('/calculate/tubeVolume', controller.tubeVolume);
router.post('/calculate/triangleArea', controller.triangleArea);

module.exports = router;