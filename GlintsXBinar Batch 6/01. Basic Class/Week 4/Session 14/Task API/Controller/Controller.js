module.exports = {
    circleArea(req, res) {
        let hasil = Math.floor(3.14 * (req.body.radius * 2));
        return res.status(200).json({
            status: true,
            result: hasil
    })
    },
    squareArea(req, res) {
        let hasil = Math.floor(req.body.side ** 2);
        return res.status(200).json({
            status: true,
            result: hasil
    })
    },
    cubeVolume(req, res) {
        let hasil = Math.floor(req.body.side ** 3);
        return res.status(200).json({
            status: true,
            result: hasil
    })
    },
    tubeVolume(req, res) {
        let hasil = Math.floor(3.14 * (req.body.radius ** 2) * req.body.height)
        return res.status(200).json({
            status: true,
            result: hasil
    })
    },
    triangleArea(req, res) {
        let hasil = Math.floor(0.5 * req.body.base * req.body.height);
        return res.status(200).json({
            status: true,
            result: hasil
    })
    }
}