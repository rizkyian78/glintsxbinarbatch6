const {product} = require('../models');
const {Op} = require('sequelize');

module.exports = {
    create(req, res) {
        product.create(req.body).then(data => {
            res.status(200).json({
                status: 'sucess',
                data: data
            }).catch(err => {
                res.status(422).json({
                    status: 'fail',
                    errors: [err.message]
                })
            })
        })
    },
    show(req, res) {
        product.findAll().then(data => {
            res.status(200).json({
                status: "success",
                data
            })
        })
    },
    findById(req, res) {
        product.findByPk(req.params.id).then(product => {
            res.status(201).json({
                status: "success",
                data: {
                    product
                }
            })
        }).catch(err => err.message) 
    },
    available(req, res) {
        product.findAll({
            where: {
                stock: {
                    [Op.gt]: 0
                }
            }
        }).then(product => {
            res.status(201).json({
                status: "sucess",
                data: {
                    product
                }
            })
        })
    },
    update(req, res) {
        product.update(req.body, {
            where: {
                id: req.body.id
            }
        }).then(message => {
            res.status(201).json({
                status: "success",
                payload: message,
               message: `Product with id ${message} is sucessfully updated` 
            })
        })
    },
    delete(req, res){
        let idProduct = {
            where: {
                id: req.params.id
            }
        }
        products.destroy(idProduct)
            .then(data => {
                res.status(200).json(data)
            })
            .catch(err => {
                res.status(404).json({
                    msg: "data is not found"
                })
            })
    }

}   