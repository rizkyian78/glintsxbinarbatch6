const {user: user} = require('../models');
const bcrypt = require('bcrypt');


module.exports = {
    register(req, res) {
        user.create({
            email: req.body.email,
            encrypted_password: req.body.password
        }).then(user => {
            res.status(200).json({
                status: "success",
                data: {
                    user
                }
            })
        }).catch(err => {
            res.status(422).json({
                status: "fail",
                errors: [err.message]
            })
        });
    },
    login(req, res) {
     user.findOne({
         where: {
             email: req.body.email.toLowerCase()
         }
     }).then(user => {
         jwt.sign(user, "secretkey", (err, token) => {
             res.status(200).json({
                 token
             })
         })
        const isPasswordTrue = bcrypt.compareSync(req.body.encrypted_password, user.encrypted_password)
        if(!user) return res.status(401).json({
                  status: "Fail",
                  error: ["Email doesn't exist"]
              })
        if(!isPasswordTrue) return res.status(401).json({
            status: "Fail",
            error: ["Salah Password"]
        })
        res.status(200).json({
            status: "Success",
            message: "Sucessfully Login"
        })
     })
     }
    }