'use strict';
const bycrypt = require('bcrypt');
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    email:{
      type: DataTypes.STRING,
      validate: {
        isEmail: true,
        isLowerCase: true
      }
    },
    encrypted_password: {
     type: DataTypes.STRING,
    //  Jika sudah ada allowNull di migrasi tidak usah
    //  allowNull: false,
     validate: {
      min: 6
    }
    } 
  },{});
  user.associate = function(models) {
    // associations can be defined here
  };
  return user;
};