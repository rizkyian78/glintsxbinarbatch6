const express = require('express');
const app = express();

const router = require('./router')

// Use MiddleWare
app.use(express.json());

app.get('/', (req, res) => {
    res.status(200).json({
        status: "sucess",
        message: "Hello World"
    })
})
// Use the router Middleware
app.use('/', router)

app.listen(3000, () => {
    console.log("Listening on Port");
})