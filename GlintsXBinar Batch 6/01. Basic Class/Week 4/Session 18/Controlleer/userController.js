const {user: user} = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')

module.exports = {
    register(req, res) {
        user.create({
            email: req.body.email,
            encrypted_password: req.body.encrypted_password
        }).then(user => {
            res.status(200).json({
                status: "success",
                data: {
                    user
                }
            })
        }).catch(err => {
            res.status(422).json({
                status: "fail",
                errors: [err.message]
            })
        });
    },
    login(req, res) {
     user.findOne({
         where: {
             email: req.body.email
         }
     }).then(user => {
        const isPasswordTrue = bcrypt.compareSync(req.body.encrypted_password, user.encrypted_password)
        if(!user) return res.status(401).json({
                  status: "Fail",
                  error: ["Email doesn't exist"]
              })
        const token = jwt.sign({
            id: user.id,
            email: user.email
        }, 'secretkey')
        if(!isPasswordTrue) return res.status(401).json({
            status: "Fail",
            error: ["Salah Password"]
        })
        res.status(200).json({
            status: "Success",
            data: {
                token
            },
            message: "Sucessfully Login"
        })
     })
     },
    //  Protected EndPoint, you must have token to
     me(req, res){
        res.status(200).json({
            status: "success",
            data: {
                user: req.user
            }
        })
     }
    }