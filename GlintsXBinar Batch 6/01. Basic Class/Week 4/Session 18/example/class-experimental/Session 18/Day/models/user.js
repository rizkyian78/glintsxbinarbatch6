'use strict';
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')
const { checkErrorCondition } = require('../helpers/error.js');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        isEmail: {
          args: true,
          msg: "Email already exist!"
        },
        isLowercase: true
      }
    },
    encrypted_password: {
      type: DataTypes.STRING,
      validate: {
        min: 6
      }
    }
  }, {
    hooks: {
      beforeValidate: instance => {
        instance.email = instance.email.toLowerCase();
      },

      beforeCreate: instance => {
        
        // Change the encrypted_password into hashed encrypted_password
        instance.encrypted_password = bcrypt
          .hashSync(instance.encrypted_password, 10)
      }
    },

    tableName: 'users'
  });

  /* Public Static Method
   * Authenticate by email and password */
  User.authenticate = async function({ email, password }) {
    /* Find the instance */
    let instance = await this.findOne({
      where: { email }
    })

    checkErrorCondition(!instance, "Email doesn't exist!");
    checkErrorCondition(!await instance.checkPassword(password), "Wrong Password");

    return Promise.resolve({
      ...instance.entity,
      token: instance.createToken()
    })
  }

  /* Public Static Method
   * Authenticate by email and password */
  User.findByToken = function(token) {
    return this.findByPk(this.decodeToken(token).id)
  }

  /* Public Static Method
   * Decoded Token */
  User.decodeToken = function(token) {
    try { return jwt.verify(token, process.env.SECRET_KEY) }
    catch { throw new Error("Invalid Token") }
  }

  /* Public Instance Method
   * Check Password */
  User.prototype.checkPassword = function(password) {
    /* Return a promise */
    return bcrypt.compare(password, this.encrypted_password); 
  }

  /* Public Instance Method
   * Generate Access Token */
  User.prototype.createToken = function() {
    return jwt.sign({ ...this.entity }, process.env.SECRET_KEY);
  }

  /* Public Instance Getter */
  Object.defineProperties(User.prototype, {
    /* Entity */
    entity: {
      get() {
        return { id: this.id, email: this.email }
      }
    }
  })

  User.associate = function(models) {
    // associations can be defined here
  };

  return User;
};
