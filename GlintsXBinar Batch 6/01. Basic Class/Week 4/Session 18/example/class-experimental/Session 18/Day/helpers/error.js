const errors = require('../lib/error.js');

module.exports = {

  /* Check if the error is actually error */
  isError: err => err instanceof Error,
  
  getMessage: err => {
    const {
      BaseError
    } = errors;

    /*
     * We will find out
     * what class the
     * error will be
     * */
    try {
      if (err instanceof BaseError) {
        return err.errors[0].message;
      }

      return err.message;
    }

    catch {
      return err.message;
    }
  },

  checkErrorCondition: (condition, message) => {
    if (condition)
      throw new Error(message);
  }
}
