const jwt = require('jsonwebtoken');
const { User } = require('../models');

module.exports = async (req, res, next) => {
  try {
    // Custom Method
    req.user = await User.findByToken(req.headers.authorization)
    next();
  }

  catch(err) {
    res.status(401);
    next(err)
  }
}
