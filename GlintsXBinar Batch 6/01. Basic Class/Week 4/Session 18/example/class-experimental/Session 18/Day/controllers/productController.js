const { product: Product } = require('../models');
const { Op } = require('sequelize');

module.exports = {

  async create(req, res, next) {
    try {
      res.status(201);
      next({ product: await Product.create(req.body) })
    }

    catch(err) { res.status(422), next(err) }
  },

  async show(req, res, next) {
    res.status(200)
    next({ products: await Product.findAll() });
  },

  async findById(req, res, next) {
    try {
      res.status(204);
      next({ product: Product.findByPk(req.params.id) })
    }

    catch(err) { res.status(400), next(err) }
  },

  async available(req, res, next) {
    res.status(200);
    next({
      products: await Product.findAll({
        where: {
          stock: { [Op.gt]: 0 }
        }
      })
    })
  },

  async update(req, res, next) {
    try {
      res.status(204);

      await Product.update(req.body, {
        where: { id: req.params.id }
      })

      next(`Product with ID ${req.params.id} is succesfully updated!`)
    }

    catch(err) {
      res.status(422);
      next(err);
    }
  }
}
