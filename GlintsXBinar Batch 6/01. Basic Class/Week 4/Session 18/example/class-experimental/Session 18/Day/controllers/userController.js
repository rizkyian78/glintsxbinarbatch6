const { User } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {

  async register(req, res, next) {
    try {
      res.status(201);
      let user = await User.create({
        email: req.body.email,
        encrypted_password: req.body.password
      })

      next({
        user: user.entity
      })
    }

    catch(err) {
      res.status(422); 
      next(err);
    }
  },

  async login(req, res, next) {
    try {
      res.status(200);
      next(
        await User.authenticate(req.body)
      )
    }

    catch(err) {
      res.status(401);
      next(err)
    }
  },

  // Protected Endpoint, you must have token to make this run
  me(req, res, next) {
    next({ user: req.user.entity });
  }

}
