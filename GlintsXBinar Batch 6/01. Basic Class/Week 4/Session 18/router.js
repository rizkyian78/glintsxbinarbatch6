const router = require('express').Router();
const productController = require('./Controlleer/controller');
const userController = require('./Controlleer/userController')
const validateMiddleware = require('./Controlleer/middleware');

// Product
router.post('/products', productController.create);
router.get('/products', productController.show);
router.get('/products-available', productController.available);
router.get('/products/:id', productController.findById);
router.put('/products/:id', validateMiddleware, productController.update);
router.delete('/products/delete/:id', validateMiddleware, productController.delete);

// User
router.post('/users/register', userController.register);
router.post('/users/login', userController.login);
router.get('/users/me', userController.me);



module.exports = router;