-- Cheat Sheet Postgre SQL--
SELECT = Memilih suatu Element
WHERE = Menyatakan Statement sesuatu bernilai benar atau salah
FROM = Menyatakan Path dari suatu File
LIKE "%pudding%" = mencari String dengan nama pudding biarpun di depan atau dibelakang dan menampilkan String
     "pudding%" = mencari String dengan nama pudding di prefix
     "%pudding" = mencari String denan nama pudding postfix
NOT = mencari baris yang tidak memiliki nilai tertentu
IS NULL = memilih baris dengan nilai null
AND = WHERE <Kondisi> AND <Kondisi2>
OR = mendapatkan kondisi salah satu harus bernilai truegit@gitlab.com:binar-x-glints-academy/batch-6/backend/materials.git
MAX() = mencari jumlah Maksimal
AVG() = Mencari Rata-Rata
GROUP BY = Mengelompokkan Baris
ORDER BY DESC = Mengurutkan dengan urutan ke bawah
GROUP BY <column_name> Having <Kondisi> = Mengelompokkan baris dengan menyatakan kondisi
SELECT <column_name> AS <column_name> = Menggantikan nama lama menjadi baru

<!-- Alter Table -->
ALTER TABLE 
[Hiearachy of SQL](./assets/HierarchyOfSQL.png) 


Note: Semua Harus Diakhiri dengan semicolon