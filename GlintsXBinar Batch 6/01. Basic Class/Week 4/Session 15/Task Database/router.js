// Route Level Middleware
const router = require('express').Router();

console.log(router);

// Import Controller
const {create, read, destroy, update, all} = require('./Controller/productController');
const { getUser, login } = require('./Controller/user')

/* Product API Collection */
router.get('/products', all);
router.post('/products', create);
router.post('/productsFilter', read);
router.put('/products', update);
router.delete('/products', destroy);

/* User API Collection */
router.get('/users', getUser);
router.post('/login', login);

module.exports = router;