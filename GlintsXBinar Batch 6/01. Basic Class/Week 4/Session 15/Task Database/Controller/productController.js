const products = require('../db/product.json');
const {product} = require('../models');


  function create(req, res) {
  product.create({
    name: req.body.name,
    price: req.body.price,
    stock: req.body.stock
  }).then(data => res.status(200).json({
    status: true,
    data
  }));
  }

  function read(req, res) {
    product.findAll({
      where: {
        name: req.body.name
      }
    }).then(data => {
      res.status(200).json({
        status: true,
        data
      })
    })
  }
  function destroy(req, res) {
    product.destroy({
      where: {
        name: req.body.name
      }
    })
    .then(data => res.status(200).json({
      status: true,
      data,
      message: "Deleted Items"
    }))
  }
  function update(req, res) {
        product.update({
          name: req.body.name,
          price: req.body.price,
          stock: req.body.stock
        },
        {
          where: {
            name: req.body.name
          }
        }
        ).then(data => res.status(200).json({
          status: true,
          data
        })).catch(err => {
          res.status(400).json({
            message: "File can't update"
          })
        })
      }
  function all(req, res) {
    product.findAll().then(data =>
      res.status(200).json({
        status: true,
        data
      })
      )
  }
  function available(req, res) {
    res.status(200).json(products.filter(i => i.stock > 0))
  }



module.exports = {
create, read, update, destroy, all, available
} 
