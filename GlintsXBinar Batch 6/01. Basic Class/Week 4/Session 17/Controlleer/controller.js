const {product} = require('../models');
const {Op} = require('sequelize');

module.exports = {
    create(req, res) {
        product.create(req.body).then(data => {
            res.status(200).json({
                status: 'sucess',
                data: data
            }).catch(err => {
                res.status(422).json({
                    status: 'fail',
                    errors: [err.message]
                })
            })
        })
    },
    show(req, res) {
        product.findAll().then(data => {
            res.status(200).json({
                status: "success",
                data
            })
        })
    },
    findById(req, res) {
        product.findByPk(req.params.id).then(product => {
            res.status(201).json({
                status: "success",
                data: {
                    product
                }
            })
        }).catch(err => err.message) 
    },
    available(req, res) {
        product.findAll({
            where: {
                stock: {
                    [Op.gt]: 0
                }
            }
        }).then(product => {
            res.status(201).json({
                status: "sucess",
                data: {
                    product
                }
            })
        })
    },
    update(req, res) {
        product.update(req.body, {
            where: {
                id: req.params.id
            }
        }).then(message => {
            res.status(201).json({
                status: "success",
                updatedData: message,
               message: `Product with id ${req.params.id} is sucessfully updated` 
            })
        })
    },
    delete(req, res){
        product.destroy({
            where: {
                id: req.params.id
            }
        }).then(product => {
            
            res.status(204).json({
                status: "success",
                message: "Item Has Deleted"  
            })
        })
    }   
}