const {user} = require('../models');
const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    let token = req.headers.authorization;
    let payload;
    try {
        payload = jwt.verify(token, "secretkey");
        //Find the useri instance and assign to the req.user
        user.findByPK({
            id: user.id
        }).then(user => {
            req.user = user;
            console.log(req.user);
        })
        next();
    } catch (err) {
        return res.status(400).json({
            status: "fail",
            errors: [
                "Invalid Token"
            ]
        })
    }
};