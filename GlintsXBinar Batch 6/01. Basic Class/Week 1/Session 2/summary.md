# How Internet Works
Today I learn About How the Internet Works there is Some Abbrevation I Know in this Session :
* ISP = Internet Service Protocol.
* DNS = Domain Name System.
* LAN = Local Area Network.
* MAC = Media Access Control.
* ARPANET = Advanced Research Protocol Agency.
* ICMP = Internet Control Message Protocol.
* URL = Uniform Resource Locator.
* HTTP = HyperText Transfer Protocol.
* HTTPS = HyperText Transfer Protocol Security.
* HTML = HyperText Markup Languange.
* SSL = Secure Socket Layer
* SSH = Secure Shell

## Internet ##
For Simplicity, Internet is a giant Network that connect with other devices in all over the world through Fiber Optic. Vint Cerf and Bob Kahn are the creator of Internet.
Originated From ARPANET. in 1970 there is no Standard Method for Network to communicate with Each Other. but in early 2000 the Internet is already booming and the world is change and there is new invention about internet that can transfer data through air called Wifi.
Wifi Work Like this :

```bash
Wifi --> ISP --> Devices
```
All the different device have unique IP Address such as :
```bash
174.129.14.120 <-- // this called IPv4 its an traditional IP adress that have 32 bits Long Designed in 1973 and adopted in 1980
3FFE:F200:0234:A1300:0123:4567:8901:ABCD <-- // this called IPv6 are modern IP adress that we use in this Era
```
aside from the IP address and DNS Work. I Have learnt how to check if my internet connection is work or not by using command:
```bash
ping www.test.com //on linux
```
and from that we get response from the DNS that we stated on the terminal

## DNS ##
DNS is a Domain name system that we always type in the browser such as www.google.com, etc. if we go in the website that we stated on the browser the computer talking to other computer or server to search for google.com and by using IPadress and we get the response from the server that we requested.
some of the Hacker use techniques DNS Spoofing by matching the domain with wrong IP adresses. and it will be dangerous.

## Other ##
Aside from the How the internet works we learn how to setup GitLab in our terminal by :

```bash
sudo apt install git //installing git through terminal on Linux
```
and using the terminal to get SSH key from the GitLab and then Paste it in the GitLab to get Identification of us.

## Issues ##
there is issues that i faced on. when i tried type ifconfig in terminal it's said "ifconfig command not found". and after i tried to comprehend the problem is by using command :
```bash
sudo apt install net-tools
```
and after that the issues have been resolved