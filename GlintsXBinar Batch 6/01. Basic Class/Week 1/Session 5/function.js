/*Multiline Comment */

// Inline Comment
function circleArea(r) {
    const phi = 3.14;
    const hasil = phi * (r**2);
    return hasil;
}
function tubeVolume(r, t) {
    let luasLingkaran = circleArea(r);
    let hasil = luasLingkaran * t;
    console.log(hasil);
};

function persegi(s) {
    let hasil = s * s;
    return hasil;
};
function kubus(s) {
    let luasPersegi = persegi(s);
    let hasil = s * luasPersegi;
    console.log(hasil)
};
const rectangleArea = (l , w) => l * w;
const blockVolume = (l, w, h) => console.log(rectangleArea(l, w) * h);

// Hasil Dari Perhitungan di atas function
blockVolume(12, 3, 5)
kubus(8)
tubeVolume(10, 2)