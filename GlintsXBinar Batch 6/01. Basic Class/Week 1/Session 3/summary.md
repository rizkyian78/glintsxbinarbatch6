## Git ##
Git is an open Source Distribution version control system. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files.
this what i know about the term in Git: 
* Control System: Used to store code.
* Version Control System: Help handling by Maintaining a history of what changes have happened.
* Distributed VCS: Stored in Server and in local Repository in the computer.

## Command In Git ##
before we work with our repository. First we must to configure our email and name to identified us with command :
```bash
git config --global user.email <email>
```
and then :
```bash
git config --global user.name <name>
```
```bash
git clone SSH/HTTPS
```
this command is to clone what repository we want.
```bash
git init
```
This command is to initialize our first project
```bash
git status
```
To check if anything in our file or repository have changed and check if we already initialized in Repository 
```bash
git add
```
to add the file and ready to commit
```bash
git checkout <branches>
```
To Switch branches
```bash
git commit -m "Message"
```
to commit the file after you add and then we can push it to Git Services for instance gitlab, bitbucket, github,
```bash
git push -u origin master
```
to push our repository to the Git Service with master branches.

## Other ##
if we have 2 or more branches, we must merge them into master branches and delete to optimize our repository.
step to push our repository into Git Services with Create new Repository :
```bash
mkdir <folder Name> => touch <File Name and Type> =>git init => git add => git commit -m "message" => git push -u origin master
```
if we have folder and push our existing folder into Gitlab Services.
```bash
cd <existing folder> => git init => git remote add origin <SSH/HTTPS> => git add => git commit -m <message you want> => git push -u origin master
```