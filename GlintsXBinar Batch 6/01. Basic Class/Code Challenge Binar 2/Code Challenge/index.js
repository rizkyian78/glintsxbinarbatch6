const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}

// Should return array
function sortAscending(data) {
  // Using Quick Sort
    data = clean(data)
  // Edge Case
    if(data.length === 1) return data;
    // Last Index of the array as Pivot
    let pivotIndex = data[data.length - 1];
    let leftArray = [];
    let rightArray = [];
    // Not included Last array to iterate
    for(let i = 0; i < data.length - 1; i++) {
        // Divide into 2 array left and right array
        if(data[i] < pivotIndex) {
            leftArray.push(data[i])
        } else {
            rightArray.push(data[i])
        }
    }
    if(leftArray.length > 0 && rightArray.length > 0) {
        // spreading the result of leftArray and right array and return recursively and the code already sorted
        return [...sortAscending(leftArray), pivotIndex, ...sortAscending(rightArray)]
    } else if(leftArray.length > 0) {
        return [...sortAscending(leftArray), pivotIndex]
    } else {
        return [pivotIndex, ...sortAscending(rightArray)]
    }
}

// Should return array
function sortDecending(data) {
  data = clean(data);
  for(let i = 0; i < data.length; i++) {
    for(let j = i + 1; j < data.length; j++) {
      if(data[i] < data[j]) {
        let temp = data[i];
        data[i] = data[j];
        data[j] = temp
      }
    }
  }
  return data;
}
// DON'T CHANGE
test(sortAscending, sortDecending, data);
