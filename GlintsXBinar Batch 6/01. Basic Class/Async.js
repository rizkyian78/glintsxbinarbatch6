// Promise
console.log("Start");
function loginUser(email, password) {
    return new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log("Have the Data");
        resolve({userEmail: email, userPassword: password});
    }, 2000);  
    })
};
function getUserVideos(email) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log("We got the Video");
            resolve(["video1","video2","video3"]);
        }, 1000);
    })
}
loginUser("Ian", "Indiarto").then(user => getUserVideos(user.email))

/*
// This is So Called Callback Hell
const user = loginUser("rizkyian78@gmail.com", 12345, user =>{
    console.log(user);
    getUserVideos(user.userEmail, video => {
        console.log(video);
    })
}) 
*/
// Async Await
const yt = new Promise(resolve => {
    setTimeout(() => {
       console.log("getting Stuff from Youtube");
       resolve({videos: [1,2,3,4]});
    }, 2000);
});
const fb = new Promise(resolve => {
    setTimeout(() => {
       console.log("getting Stuff from Facebook");
       resolve({user: "Ian Indiarto"}) 
    }, 2000);
})

async function displaysUser() {
    try {
        const loggedUser = await loginUser('ian', 12345);
        const videos = await getUserVideos(loggedUser.userEmail);
        console.log(videos);
    } catch (error) {
        console.log("cannot get the videos");
    }
}
displaysUser();

Promise.all([yt, fb]).then(result => console.log(result));
console.log("end");