```javascript
// Initilizing The Function
function greet(name, address, birthday) {
    // Generate a New Date in this current Date, Month and Year
      const currentDate = new Date();
      //I want to know what happen in currentDate by assign it to my terminal. it yield "2020-04-24T12:29:22.092Z"
      console.log(currentDate);
      // Assign a new Value to get current Year using Method new Date and get the Fullyear Method
      const currentYear = currentDate.getFullYear();
      // I want to know How the Methods Works and What the Methods yield when i Console.log. and it yield: 2020 it means current year
      console.log(currentYear);
      //Seeking the age value by substract currentYear and Birthday Input and reassign as a modified value
      birthday = currentYear - birthday;
      // By Assigning Dollar Sign and backtick i can access my name, birthday,and Address. i think backtick is more convenient
      console.log(`Hello, ${name} Looks like you're ${birthday} and you Lived in ${address}`)
      // 2nd Option To get space I use " " it means double quotes with blank space
      console.log("Hello, " + name + " " + "Looks like you're " + birthday + " years Old " + "And you Lived in " + address );
  };
    //For The Reminder we must input the value orderly like in the Parameter/ Arguments 
  greet("Rizky Ian Indiarto", "Malang", 1995)
  ```
