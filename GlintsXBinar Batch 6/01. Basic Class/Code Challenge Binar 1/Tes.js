var maxProfit = function(prices) {
  if (prices.length < 2) return 0;
const len = prices.length;
const left = new Uint16Array(len);
const right = new Uint16Array(len);
let low = prices[0];
let high = prices[len - 1];
let mleft = mright = 0;
for (let i = 1; i < len; ++i) {
  prices[i] < low
    ? (low = prices[i])
    : prices[i] - low > mleft && (mleft = prices[i] - low);
  left[i] = mleft;
  const idxRight = len - i - 1;
  prices[idxRight] > high
    ? (high = prices[idxRight])
    : high - prices[idxRight] > mright && (mright = high - prices[idxRight]);
    console.log(mright)
    right[i] = mright;
}

let max = 0;
for (let i = 0; i < len; ++i) {
  const sum = left[i] + right[len - i - 1];
  sum > max && (max = sum);
}
return max;
};

maxProfit([3,3,5,0,0,3,1,4])
