const {Post} = require('../models');
const {User} = require('../models')

const present = require('../middlewares/userHelper');

exports.create = async function(req, res, next) {
    try {
        const post = await Post.create({
            title: req.body.title,
            body: req.body.body,
            user_id: req.user.id
        })
        present(res, post, 201, "Post")
    } catch (err) {
        res.status(422);
        next(err)
    }
}
exports.all = async function(req, res, next) {
    const posts = await Post.findAll({
        include: 'mbut'
    });
    res.data = {posts};
    console.log(res.data.posts.Post);
    present(res, posts, 200, "posts")
}

exports.update = async function(req, res, next) {
        try {
        if(paramsId.user_id == req.user.id) {
            const {title, body} = req.body;
            await Post.update({title, body}, {
                where: {
                    id: req.params.id
                }
            })
            res.status(200).json({
                status: "Success",
                Message: `id ${req.params.id} has been updated`
            })
        } else {
            res.status(404).json({
                status: "Fail",
                Message: "Not Found"
            })
        }
        } catch (err) {
            res.status(422);
            next(err)
        }
    // try{errorerror
    //      await Post.update(req.body, {
    //         where: {id: req.params.id}
    //     })
    //     present(res, "Success", 200, "message")
    // } catch(err) {
    //     res.status(422);
    //     next(err)
    // }
}

exports.delete = async function(req, res, next) {
    try {
        if(User.user_id == res.user.id) {
            await Post.destroy({
                where: {
                    id: req.params.id
                }
            })
            res.status(200).json({
                status: "Success",
                Message: `${req.params.id} already been deleted`
            })
        }
    } catch (err) {
        res.status(500).json({
            status: "Fail",
            Message: `${err}`
        })
    }
}
