const router = require('express').Router();
const productController = require('./Controlleer/controller');
const userController = require('./Controlleer/userController')
const validateMiddleware = require('./Controlleer/middleware');
const postController = require('./Controlleer/postController');

// Product
router.post('/products', productController.create);
router.get('/products', productController.show);
router.get('/products-available', productController.available);
router.get('/products/:id', productController.findById);
router.put('/products/:id', validateMiddleware, productController.update);
router.delete('/products/delete/:id', validateMiddleware, productController.delete);

// User
router.post('/users/register', userController.register);
router.post('/users/login', userController.login);
router.get('/users/me', userController.me);
router.post('/users/profile',validateMiddleware, userController.createProfile)

// POST API
router.post('/posts',validateMiddleware, postController.create);
router.get('/posts', postController.all)
router.put('/posts/:id', postController.update);
router.delete('/posts/:id', postController.delete);


module.exports = router;