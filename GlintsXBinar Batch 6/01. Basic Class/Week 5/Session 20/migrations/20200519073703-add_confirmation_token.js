'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('users', 'confirmation_token', {
        type: Sequelize.STRING,
        allowNull: true
      }),
      queryInterface.addColumn('users', 'fcm_token')
    ])
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('users', 'confirmation_token');
  }
}
