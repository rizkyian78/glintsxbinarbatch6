'use strict';
module.exports = (sequelize, DataTypes) => {
  const Post = sequelize.define('Post', {
    title: DataTypes.STRING,
    body: DataTypes.TEXT,
    user_id: DataTypes.INTEGER
  }, {});
  Post.associate = function(models) {
    Post.belongsTo(models.user, {
      foreignKey: 'user_id',
      as: 'mbut'
    })
  };
  return Post;
};