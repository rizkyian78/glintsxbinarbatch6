const express = require('express');
const app = express();
const router = require('./router');
const morgan = require('morgan');
const base = require('./middlewares/baseHandler');

// Load .env
require('dotenv').config();

// Use middleware
app.use(morgan('dev'));
app.use(express.json());
app.get('/', (req, res) => {
  res.status(200).json({
    status: "success",
    message: "Hello World"
  })
})
// Use the router middleware
app.use('/', router);

base.forEach(handler =>
  app.use(handler)
);

app.listen(3000, () => {
  console.log("Listening on port 3000!");
})
