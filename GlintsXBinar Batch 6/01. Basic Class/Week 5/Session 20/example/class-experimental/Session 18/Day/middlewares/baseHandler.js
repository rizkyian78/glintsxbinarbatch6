const {
  isError,
  getMessage
} = require('../helpers/error.js');

module.exports = [
  // 404 Not Found Handler
  function(req, res, next) {
    res.status(404);
    next(new Error("Not found"))
  },

  // Response Formatter
  function(paramsThatBeingPassedFromNextFunction, req, res, next) {
    /*
     * Check if the params that being passed from next function is actually an instance of Error
     * If it is, then all we need to do is telling express to the next middleware and serve the Error Response
     * */
    if (isError(paramsThatBeingPassedFromNextFunction))
      return next(paramsThatBeingPassedFromNextFunction);
    /* Otherwise, just send the response */
    res.json({
      status: 'success',
      data: paramsThatBeingPassedFromNextFunction
    })
  },

  function(err, req, res, next) {
    /*
     * If it's an error an the response code still 200
     * it means that it is an unhandled error!
     * So we need to change the response code into 500
     * */
    if (res.statusCode == 200) {
      res.status(500);
      console.log(
        "Errors:",
        getMessage(err)
      );
    }

    // Send JSON Response
    res.json({
      status: 'fail',
      errors: [getMessage(err)]
    })
  }
]
