const router = require('express').Router();
const productController = require('./controllers/productController');
const userController = require('./controllers/userController');
const authenticate = require("./middlewares/authenticate");

/* Product API Collection */
router.post('/products', authenticate, productController.create);
router.get('/products', productController.show);
router.get('/products/available', productController.available);
router.get('/products/:id', productController.findById);
router.put('/products/:id', authenticate, productController.update);

/* User API Collection */
router.post('/users/register', userController.register);
router.post('/users/login', userController.login);
router.get('/users/me', authenticate, userController.me);

module.exports = router;