const jwt = require('jsonwebtoken');
const { User } = require('../models');

module.exports = async (req, res, next) => {
  /* Share the same req and res object. */
  try {
    let token = req.headers.authorization;
    let payload = await jwt.verify(token, process.env.SECRET_KEY);
    req.user = await User.findByPk(payload.id);
    next();
  }

  catch {
    res.status(401);
    next(new Error("Invalid Token"))
  }
}