const {user: User} = require('../models');
const jwt = require('jsonwebtoken');

module.exports = async (req, res, next) => {
    try {
        let token = req.headers.authorization;
        payload = await jwt.verify(token, "secretkey");
        let user = await User.findByPk(payload.id)
        req.user = user;
        next();
    } catch (err) {
        res.status(401)
        next(new Error("Error"))
    }
}