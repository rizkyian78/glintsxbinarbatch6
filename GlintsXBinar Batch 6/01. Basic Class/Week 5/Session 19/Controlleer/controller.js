const {product} = require('../models');
const {Op} = require('sequelize');

const Product = require('../middlewares/helper')

module.exports = {
    create(req, res) {
        product.create(req.body).then(data => Product(res, 200, data))
        .catch(err => {
            res.status(422).json({
                status: 'fail',
                errors: [err.message]
            })
        })
    },
    show(req, res) {
        product.findAll().then(data => Product(res, 200, data))
    },
    findById(req, res) {
        product.findByPk(req.params.id).then(data => Product(res, 201, data)).catch(err => err.message) 
    },
    available(req, res) {
        product.findAll({
            where: {
                stock: {
                    [Op.gt]: 0
                }
            }
        }).then(data => Product(res, 201, data) )
    },
    update(req, res, next) {
        product.update(req.body, {
            where: {
                id: req.params.id
            }
        }).then(data => Product(res, 201, `Id ${req.params.id} has been updated`));
    },
    delete(req, res){
        product.destroy({
            where: {
                id: req.params.id
            }
        }).then(data => Product( res, 201, `Deleted item with id ${req.params.id}`))
    }   
}