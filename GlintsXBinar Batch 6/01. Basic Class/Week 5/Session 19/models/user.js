'use strict';
const bycrypt = require('bcrypt');
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    email: {
     type: DataTypes.STRING,
      unique: true,
      validate: {
        isEmail: true,
        isLowercase: true
      }
    },
    encrypted_password: DataTypes.STRING
  },{
    hooks: {
      beforeValidate: instance => {
        instance.email = instance.email.toLowerCase();
      },
      beforeCreate: (instance, option) => {
        const salt = bycrypt.genSaltSync(10)
        instance.encrypted_password = bycrypt.hashSync(instance.encrypted_password, salt)
      }
    }
  });
  user.associate = function(models) {
    user.hasMany(models.Post, {
      foreignKey: 'user_id',
      as: 'mbut'
    })
  };
  return user;
};