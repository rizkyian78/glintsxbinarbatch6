'use strict';
module.exports = (sequelize, DataTypes) => {
  const Profile = sequelize.define('Profile', {
    bio: DataTypes.TEXT,
    phone_number: DataTypes.STRING,
    address: DataTypes.TEXT,
    photo: DataTypes.TEXT,
    user_id: DataTypes.INTEGER
  }, {});
  Profile.associate = function(models) {
    // associations can be defined here
  };
  return Profile;
};