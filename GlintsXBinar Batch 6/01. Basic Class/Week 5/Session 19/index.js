const express = require('express');
const app = express();

const router = require('./router')
const middleware = require('./Controlleer/middleware')
const exception = require('./middlewares/Execptionhandler');
const morgan = require('morgan');

// Connect to Sqlectron

// Use MiddleWare
app.use(morgan('tiny'));
app.use(express.json());
// app.use(middleware);

app.get('/', (req, res) => {
    res.status(200).json({
        status: "sucess",
        message: "Hello World"
    })
})
// Use the router Middleware
app.use('/', router)
// Error Handler
exception.forEach(handler => app.use(handler))

app.listen(3000, () => {
    console.log("Listening on Port");
})