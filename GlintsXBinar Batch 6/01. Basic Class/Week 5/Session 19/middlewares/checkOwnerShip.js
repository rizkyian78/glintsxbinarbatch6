const models = require('../models');

module.exports = modelsName => {
    const model = models[modelsName];
    return async function(req, res, next) {
        try {
            let instance = await model.findByPk(req.params.id);
            if(instance.user_id !== req.user.id) {
                res.status(403);
                return next(new Error(`This is not your ${modelsName}`))
            }
            next();
        } catch (err) {
            res.status(400);
            next(err)
        }
    }
}