module.exports = (res, data, code, key) => {
    res.status(code).json({
        status: "Success",
        data: {
            [key]: data
        }
    })
}