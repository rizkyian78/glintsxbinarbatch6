module.exports = {
  /* Built-In Error */
  Error,

  /* Sequelize Error */
  BaseError: require('sequelize').BaseError,
}
