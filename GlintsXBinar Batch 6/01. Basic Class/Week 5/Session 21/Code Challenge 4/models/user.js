'use strict';

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      validate: {
        isLowercase: true,
        isEmail: true,
        notEmpty: true
      }
    },
    encrypted_password: {
      type: DataTypes.STRING,
      validate: {
        min: 6,
        notEmpty: true
      }
    },
    verified: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    confirmation_token: {
      type: DataTypes.STRING
    },
    role: {
      type: DataTypes.ENUM('admin', 'member'),
      defaultValue: 'member',
      allowNull: false
    }
  }, {
    underscored: true,
  });

  User.associate = function(models) {
    User.hasMany(models.Post, {
      foreignKey: 'user_id'
    })
  };

  // Public Prototype Getter
  Object.defineProperty(User.prototype, 'entity', {
    get() {
      return {
        id: this.id,
        email: this.email,
        verified: this.verified,
        role: this.role,
        access_token: this.getToken() 
      }
    }
  });

  /* Public Static Authenticate Method */
  User.authenticate = async function({ email, password }) {
    try {
      let instance = await this.findOne({
        where: { email: email.toLowerCase() }
      })
      if (!instance) return Promise.reject(new Error("Email doesn't exist"));
      
      let isValidPassword = instance.checkCredential(password);
      if (!isValidPassword) return Promise.reject(new Error("Wrong password!"));
    
      return Promise.resolve(instance);
    }

    catch(err) {
      return Promise.reject(err);
    }
  }
  /* End of Authenticate Method */

  /* Public checkCredential Method */
  User.prototype.checkCredential = function(password) {
    /* It will return true or false
      Depending on the result of bcrypt.compareSync */
    return bcrypt.compareSync(password, this.encrypted_password);
  }
  /* End of checkCredential Method */

  /* Public getToken Method */
  User.prototype.getToken = function() {
    return jwt.sign({
      id: this.id,
      email: this.email,
      veriified: this.verified
    }, process.env.SECRET_KEY);
  }

  /* Hooks */
  User.beforeCreate(user => {
    const hash = bcrypt.hashSync(user.encrypted_password, 10);
    user.encrypted_password = hash;

    /* If your app doesn't allow you to change the email,
       Then it is fine if you want to use this hook. */
    user.email = user.email.toLowerCase();
  })
  /* End of Hooks */

  return User;
};