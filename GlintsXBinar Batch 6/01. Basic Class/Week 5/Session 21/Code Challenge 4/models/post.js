'use strict';
module.exports = (sequelize, DataTypes) => {
  const Post = sequelize.define('Post', {
    title: DataTypes.STRING,
    body: DataTypes.TEXT,
    approved: DataTypes.BOOLEAN,
    user_id: DataTypes.INTEGER
  }, {});

  Post.associate = function(models) {
    Post.belongsTo(models.User, {
      foreignKey: 'user_id',
      as: 'author'
    })
  };
  return Post;
};