const express = require('express');
const app = express()
const morgan = require('morgan');

/* Server Configuration */
const dotenv = require('dotenv');
dotenv.config(); // Read .env and set it as Environment Variable

const router = require('./router.js'); // Import router
const errorHandler = require('./middlewares/errorHandler') // Import Error Handler Middleware

// Middleware to Parse JSON
app.use(morgan('tiny'));
app.use(express.json());

// Debug mode setting
if (process.env.DEBUG == "true")
  app.use((req, res, next) => {
    console.log('Request:', req.body);
    next();
  })
 
app.get('/', function (req, res) {
  res.json({
    status: true,
    message: 'Hello World'
  })
});
app.get('/test', function(req, res) {
  console.log(req.body);
  res.end();
})

app.use('/api/v1', router);

// Error Handler Middleware
errorHandler.forEach(handler => app.use(handler));

app.listen(3000, () => {
  console.log("Listening on port 3000!");
})