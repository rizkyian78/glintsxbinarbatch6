const { User, Profile } = require('../models');
const present = require('../helpers/responseFormatter');

module.exports = {

  async register(req, res) {
    /*
      We can't save user's password as plain text
      We should encrypt that password
      before saving it to the DB.
      For safety reason.
    */

    req.body.email = req.body.email || "";

    try {
      let user = await User.create({
        email: req.body.email.toLowerCase(),
        encrypted_password: req.body.password
      })

      // Call response formatter helper
      present(res, user, 201, 'user');
    }

    catch(err) {
      next(err);
    }
  },

  async login(req, res, next) {
    try {
      let user = await User.authenticate(req.body);
      present(res, user.entity, 201, 'user');
    }

    catch(err) {
      res.status(401);
      next(err);
    }

  },

  me(req, res) {    
    present(res, req.user.entity, 200, 'user');
  },

  async createProfile(req, res, next) {
    try {
      let profile = await Profile.create({
        bio: req.body.bio,
        phone_number: req.body.phone_number,
        address: req.body.address,
        user_id: req.user.id
      })
  
      present(res, profile, 201, 'profile')
    }

    catch(err) {
      res.status(400);
      next(err);
    }

  },

  alterToAdmin(req, res, next) {
    if (req.headers["secret-key"] == process.env.ADMIN_SECRET_KEY) {
      req.user.update({ role: 'admin' });
      present(res, req.user.entity, 200, 'user');
    } else {
      res.status(403)
      next(new Error("You can't do this!"))
    }
  }

}