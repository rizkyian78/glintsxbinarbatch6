const { Post, User } = require('../models');
const present = require('../helpers/responseFormatter');

exports.create = async function(req, res, next) {
  try {
    const post = await Post.create({
      title: req.body.title,
      body: req.body.body,
      user_id: req.user.id
    });

    present(res, post, 201, 'post');
  }

  catch(err) {
    res.status(422);
    next(err);
  }
}

exports.all = async function(req, res, next) {
  let query = {};

  console.log(req.user.role);
  if (req.user.role === 'member') query = { approved: true };

  const posts = await Post.findAll({
    include: 'author',
    where: query
  })

  present(res, posts, 200, 'posts');
}

exports.update = async function(req, res, next) {
  try {
    await Post.update(req.body, {
      where: { id: req.params.id }
    })

    present(res, "Successfully updated", 200, 'message')
  }

  catch(err) {
    res.status(422);
    next(err);
  }
}

exports.delete = async function(req, res, next) {
  try {
    await Post.destroy({
      where: { id: req.params.id }
    })

    present(res, "Successfully removed", 200, 'message')
  }

  catch(err) {
    res.status(422);
    next(err);
  }
}

exports.approve = async function(req, res, next) {
  await Post.update({ approved: true }, {
    where: { id: req.params.id }
  })
  present(res, "Succesfully updated!", 200, "message");
}

exports.deleteFromAdmin = async function(req, res, next) {
  try {
    await Post.destroy({
      where: { id: req.params.id }
    })

    present(res, "Successfully removed", 200, 'message')
  }

  catch(err) {
    res.status(422);
    next(err);
  }
}