const router = require('express').Router();

// Middlewares
const authenticate = require('./middlewares/authenticate');
const checkOwnership = require('./middlewares/checkOwnership');
const isAdmin = require('./middlewares/isAdmin');

// Controller
const userController = require('./controllers/userController');
const postController = require('./controllers/postController');

// User API Collection
router.post('/users/register', userController.register);
router.post('/users/login', userController.login);
router.get('/users/me', authenticate, userController.me)
router.post('/users/profile', authenticate, userController.createProfile);
router.put('/users/alter-to-admin', authenticate, userController.alterToAdmin);

// Post API Collection
router.post('/posts', authenticate, postController.create);
router.get('/posts', authenticate, postController.all);
router.put('/posts/:id', authenticate, checkOwnership('Post'), postController.update);
router.delete('/posts/:id', authenticate, checkOwnership('Post'), postController.delete);
router.put('/posts/:id/approve', authenticate, isAdmin, postController.approve);
router.delete('/admin/posts/:id', authenticate, isAdmin, postController.deleteFromAdmin);

module.exports = router;