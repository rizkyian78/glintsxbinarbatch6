module.exports = (req, res) => {
  if (res.statusCode >= 200 && res.statusCode < 300) {
    res.json({
      status: 'success',
      data: res.data
    })
  } else {
    res.json({
      status: 'fail',
      errors: 'Ini error'
    })
  }
}