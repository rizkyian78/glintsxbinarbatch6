/* Using function to format the response,
   and there's another way */

module.exports = (res, data, statusCode, key) => {
  res.status(statusCode).json({
    status: 'success',
    data: {
      [key]: data
    }
  })
}