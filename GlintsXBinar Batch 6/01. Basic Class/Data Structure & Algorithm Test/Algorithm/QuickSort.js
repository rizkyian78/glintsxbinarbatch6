const data = [];
const randomNumber = Math.floor(Math.random() * 100);

function createArray() {
    for (let i = 0; i < randomNumber; i++) {
      data.push(createArrayElement())
    }
}
const quickSort = data => {
    // Edge Case
    if(data.length === 1) return data;
    // Last Index of the array
    let lastIndex = data[data.length - 1];
    let leftArray = [];
    let rightArray = [];
    // Not included Last array to iterate
    for(let i = 0; i < data.length - 1; i++) {
        // Divide into 2 array left and right array
        if(data[i] < lastIndex) {
            leftArray.push(data[i])
        } else {
            rightArray.push(data[i])
        }
    }
    if(leftArray.length > 0 && rightArray.length > 0) {
        // spreading the result of leftArray and right array and return recursively
        return [...quickSort(leftArray), lastIndex, ...quickSort(rightArray)]
    } else if(leftArray.length > 0) {
        return [...quickSort(leftArray), lastIndex]
    } else {
        return [lastIndex, ...quickSort(rightArray)]
    }
}

console.log(quickSort(data)); 