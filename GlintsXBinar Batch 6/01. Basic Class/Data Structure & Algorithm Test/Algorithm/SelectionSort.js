let array = [6, 4, 10, 33, 3,-2, 21, 1, -1]

const selectionSort = data => {
    for(let i = 0; i < data.length -1; i++) {
        // Assume 1st element is the Smallest
        let minIndex = i;
        // Look for minimum element
        for(let j = i + 1; j < data.length; j++) {
            // Compare each element of j with element minIndex
            if(data[j] < data[minIndex]) {
                // assign minIndex with index of j
                minIndex = j;
            }
        }
        //Swap the elements of data[i] with element data[minIndex]
        let temp = data[i];
        data[i] = data[minIndex]
        data[minIndex] = temp
    }
    console.log(data)
}

selectionSort(array)