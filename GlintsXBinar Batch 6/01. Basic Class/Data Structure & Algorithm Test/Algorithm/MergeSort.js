let array = [6, 4, 10, 33, 3,-2, 21, 1, -1];

// Helper function to merge the array
const helperFunction = (leftArray, rightArray) => {
    // Store the Element
    let output = [];
    // left index start 0
    let leftIndex = 0;
    // right index start at 0
    let rightIndex = 0;
    // condition break leftIndex and right Index until leftArray.length and rightArray.length
    while(leftIndex < leftArray.length && rightIndex < rightArray.length) {
        // Store left Element into left element
        let leftElement = leftArray[leftIndex];
        // Store right Element into right element
        let rightElement = rightArray[rightIndex];
        // Check the Condition
        if(leftElement < rightElement) {
            output.push(leftElement);
            leftElement++
        } else {
            output.push(rightElement)
            rightElement++
        }
    }
    return output.concat(leftArray.slice(leftIndex)).concat(rightArray.slice(rightIndex))
}

const mergeSort = data => {
    if(data.length <= 1) {
        return data
    }
    // divide Index by 2 using Math.floor
    let middleIndex = Math.floor(data.length / 2);
    // Store array into leftArray Start from Index 0 to middleIndex / Index 4
    let leftArray = data.splice(0, middleIndex);
    // Store array into rightArray Start from middleIndex to end of the array
    let rightArray = data.splice(middleIndex)
    // Recursive by divide the stored array turn into just 1 element
    return helperFunction(
        mergeSort(leftArray),
        mergeSort(rightArray)
    )
}

console.log(mergeSort(array));