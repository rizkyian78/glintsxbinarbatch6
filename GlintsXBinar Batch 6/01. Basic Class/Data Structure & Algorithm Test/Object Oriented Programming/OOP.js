//Contoh Normal Function
let baseSalary = 30_000;
let overtime = 10;
let rate = 20

function getWage(baseSalary, overtime, rate) {
    return baseSalary + (overtime * ratae)
}

// Contoh OOP (Encapsulation)
let employee = {
    baseSalary: 30_000,
    overtime: 10,
    rate: 20,
    getWage: () => {
        return this.baseSalary + (this.overtime * this.rate)
    }
}
employee.getWage()

// Contoh OOP (Abstraction)
let 