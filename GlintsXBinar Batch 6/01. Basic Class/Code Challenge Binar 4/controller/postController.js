const {posts, users} = require('../models');

exports.create = async function(req, res) {
    try {
        const post = await posts.create({
            title: req.body.title,
            body: req.body.body,
            user_id: req.user.id
        })
        res.status(200).json({
            status: "Okay",
            data: post  
        })
    } catch (err) {
        res.status(422).json({
            status: "Fail"
        })
    }
};
exports.getAll = async function(req, res) {
    try {
        const user = await posts.findOne({
            where: {
                id: req.user.id
            }
        })
        if(user.roles === 'Admin') await posts.findAll();       
        if(user.roles === 'Member') await posts.findAll({
            where: {approved: true}
        })
        res.status(200).json({
            status: "Success",
            data
        })
    } catch (err) {
        res.status(500).json({
            status: "Fail",
            Message: "Can't get Posts"
        })       
    }
}
exports.update = async function(req, res){
    try {
        const user = await users.findOne({
            where: {
                id: req.user.id
            }
        })
        if(user.roles === 'Admin') {
            // it's still not complete idk if this can work or not
            await posts.update(req.body.approved, {where: {id: req.params.id}})
        } 
    // const userPost = posts.findOne({where: {id: req.params.id}})
    await posts.update(req.body, {where: {id: req.params.id}});
    res.status(200).json({
       status: "Success",
        Message: `Id ${req.params.id} already updated`
    })
    } catch (err) {
        res.status(422).json({
            status: "Fail",
            Message: `Id ${req.params.id} `
        })
    }
}
exports.delete = async function(req, res) {
    try {
        const user = await users.findOne({
            where: {
                id: req.user.id
            }
        })
        if(user.roles === "Admin") {
            await posts.destroy({where: {id: req.params.id}})
        }
        await posts.destroy({where: {id: req.params.id}});
        res.status(200).json({
            status: "Success",
            Message: `id ${req.params.id} has been deleted`
        })
    } catch (error) {
        res.status(422).json({
            status: "Fail",
            Message: `Unauthorized`
        })       
    }
}