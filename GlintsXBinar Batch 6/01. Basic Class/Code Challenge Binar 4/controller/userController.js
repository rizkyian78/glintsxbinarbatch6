const {users} = require('../models');
const jwt = require('jsonwebtoken');
const bycrypt = require('bcrypt');

exports.register = async function(req, res, next){
    try {
        const user = await users.create({
            email: req.body.email,
            password: req.body.password,
            roles: req.body.roles
        })
        res.status(200).json({
            status: "Success",
            data: user
        })
    } catch (error) {
        console.log(error)
        res.status(400).json({
            status: "Fail",
        })       
    }
}
exports.login = async function(req, res, next) {
    try {
        const user = await users.findOne({
            where: {email: req.body.email}
        })
        const isPasswordTrue = await bycrypt.compare(req.body.password, user.password);
        if(!user) return res.status(401).json({
            status: "Fail",
            Message: "Unauthorized User"
        })
        const token = await jwt.sign({
            id: user.id,
            email: user.email
        }, "rahasia")
        if(!isPasswordTrue) return res.status(401).json({
            status: "Fail",
            Message: "Wrong Password"
        })
        res.status(200).json({
            status: "Success",
            token,
            Message: `${req.body.email} has been login`
        })
    } catch (error) {
        res.status(401).json({
            status: "Success",
            Message: `Unauthorized`
        })
    }
}