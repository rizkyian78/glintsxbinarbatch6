const router = require('express').Router();
const postController = require('./controller/postController');
const userController = require('./controller/userController');
const validateMiddleware = require('./Middleware/validateMiddleware')
const checkOwnerShip = require('./Middleware/checkOwnerShip')

// Post API
router.post('/posts', validateMiddleware, postController.create);
router.get('/posts', postController.getAll);
router.put('/posts/:id',validateMiddleware,checkOwnerShip('posts'), postController.update);
router.delete('/posts/:id',validateMiddleware,checkOwnerShip('posts'), postController.delete);

// Users API
router.post('/users/register', userController.register);
router.post('/users/login', userController.login);

module.exports = router;