'use strict';
const bycrypt = require('bcrypt')
module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('users', {
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    roles: {
      type: DataTypes.STRING,
      defaultValue: "Member"
    }
  }, {
    hooks: {
        beforeValidate: instance => {
          instance.email = instance.email.toLowerCase();
        },
        beforeCreate: instance => {
          const salt = bycrypt.genSaltSync(10)
          instance.password = bycrypt.hashSync(instance.password, salt)
        }
    }
  });
  users.associate = function(models) {
    // associations can be defined here
  };
  return users;
};