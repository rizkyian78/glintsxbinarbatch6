'use strict';
module.exports = (sequelize, DataTypes) => {
  const posts = sequelize.define('posts', {
    title: DataTypes.STRING,
    body: DataTypes.STRING,
    approved: DataTypes.BOOLEAN,
    user_id: DataTypes.INTEGER
  }, {});
  posts.associate = function(models) {
    // associations can be defined here
    posts.belongsTo(models.users, {
      foreignKey: 'user_id',
      as: 'author'
    })
  };
  return posts;
};