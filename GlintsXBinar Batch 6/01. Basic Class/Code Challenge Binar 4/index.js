const express = require('express');
const morgan = require('morgan');
const app = express();
const router = require('./router');
const PORT = 3000;

app.use(morgan('tiny'));
app.use(express.json());

app.use('/', router)
app.use('/', (req, res,) => {
    console.log("hello");
});
app.listen(PORT, () => {
    console.log(`Listening On Port ${PORT}`);
})