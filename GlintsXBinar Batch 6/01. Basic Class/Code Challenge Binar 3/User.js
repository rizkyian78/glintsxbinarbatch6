// Helper OOP
const fs = require('fs')
const path = require('path')
const user = require('./User.json')
class User {
  
    // Filepath
    static get filepath() {
      // to /home/toshiba/glintsxbinarbatch6/GlintsXBinar Batch 6/01. Basic Class/Code Challenge 3/${Sesuai dengan nama class}.json
      return path.resolve(
        __dirname,
        `${this.name}.json`
      )
    }
  
    // Get All Data
    static get all() {
      try {
        return eval(
          fs.readFileSync(user)
            .toString()
        )
      }
      catch {
        return []
      }
    }
  
    _checkUnique({ key, value }) {
      let query = {}
      query[key] = value;
      console.log(query[key]);
      if (this._find(query).length !== 0)
        throw new Error(`${key} already exists`);
    }
    fetchData(props) {
        fetch('./User.json')
        .then(function(res) {
            return res.json();
        })
        .then(function(data) {
            console.log(data);
        })
    }
  }

  module.exports = User;