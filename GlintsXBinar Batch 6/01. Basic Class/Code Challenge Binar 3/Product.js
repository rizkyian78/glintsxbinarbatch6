const fs = require('fs');
const path = require('path');

class Product {
  
  // Filepath
  static get filepath() {
    console.log(path.resolve(
      __dirname,
      `${this.name}.json`
    ));
    // to /home/toshiba/glintsxbinarbatch6/GlintsXBinar Batch 6/01. Basic Class/Code Challenge 3/Product.json
    return path.resolve(
      __dirname,
      `${this.name}.json`
    )
  }

  // Get All Data
  static get all() {
    try {
      return eval(
        fs.readFileSync(this.filepath)
          .toString()
      )
    }
    catch {
      return []
    }
  }

  _checkUnique({ key, value }) {
    let query = {}
    query[key] = value;
    console.log(query[key]);
    if (this._find(query).length !== 0)
      throw new Error(`${key} already exists`);
  } 
}

module.exports = Product;