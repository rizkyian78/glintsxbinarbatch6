const Products = require('./Product');
const user = require('./User.json');

/*
  Code Challenge #3

  Your goals to create these endpoint
    
    /products
      This will show all products on products.json as JSON Response

    /products/available
      This will show the products which its stock more than 0 as JSON Response

    /users
      This will show the users data inside the users.json,
      But don't show the password!

  */

const http = require('http');
const port = 3000
const server = http.createServer(function(req, res) {
  switch (req.url) {
      case '/':
        res.write('<html>');
        res.write('<head><title>Welcome</title></head>')
        res.write('<body>')
        res.write('<h1><a href="/products">Go to Product Page</h1>')
        res.write('<h1><a href="/products/available">Go to Product Available Page</h1>')
        res.write('<h1><a href="/user">Go to User Page</h1>')
        res.write('</body>')
        res.write('</html>');
          break;
      case '/products':
          res.write(JSON.stringify(Products.all))
          break;
      case '/products/available':
                  /*
                  penggunaan loop dan ternyata harus di push
                  let temp = [];
                  for(let key of Product.all) {
                  if(key.stock > 0) {
                  temp.push(key)
                    }
                  }
                  console.log(temp); */
                  // Penggunaan menggunakan filter
                  res.write(JSON.stringify(Products.all.filter( i => i.stock != 0)))
            break;
      case '/user':
        for(let prop in user) {
          if(prop == 'password') {
            delete user.password
            console.log(user);
          }
        }
        res.write(JSON.stringify(user))
        break;
      default:
          res.writeHead(404)
          res.write("404 Not found")
          break;
  }
  res.end(); // Close the
});

server.listen(port, () => {
  console.log(`Server listen at ${port}`);
})

/* Code Here */


// Boleh Kenalan sama namanya Sabrina di Binar Mas? WKWKWKWKWKWK