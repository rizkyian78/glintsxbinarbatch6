const http = require('http');
const Products = require('../../Session 12/Product')
const app = http.createServer(function(req, res) {
    switch (req.url) {
        case '/':
            res.write("Babi")
            break;
        case '/products': 
            res.write(JSON.stringify(Products.all))
            break;
        case '/error':
            try {
                throw new Error("Whoopsie");
            } catch (error) {
                res.writeHead(500);
                res.write(error.message)
            }
        default:
            res.writeHead(404)
            res.write("404 Not found")
            break;
    }
    res.end(); // Close the
});

const PORT = 8000

app.listen(PORT, () => console.log(`Listening Port ${PORT}`))