const Record = require('./Record');

class Product extends Record {
  static properties = {
    name: {
      type: 'string',
      required: true
    },
    price: {
      type: 'number',
      required: true
    },
    stock: {
      type: 'number',
      required: true
    },
  }
}
console.log(Product.constructor.call);

module.exports = Product;
