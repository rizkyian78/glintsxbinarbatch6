/*
  Abstract Class is a class where
  we can't instantiate an instance from it,
  unless it is instantiated from the sub class of that class.

  That means, everything that were defined inside the Abstract Class,
  were meant to be implemented by the sub class.

  So, abstract class is basically just a blueprint of another class
  */

// Abstract Class
class Human {
  
    static properties = [
      "name",
      "gender",
    ]
  
    constructor(props) {
      this._validate(props);
  
      /* Instance Properties */
      this.name = props.name;
      this.gender = props.gender;
    }
  
    _validate(props) {
      if (this.constructor === Human)
        throw new Error("Human is an abstract class!");
  
      if (typeof props !== 'object' || Array.isArray(props))
        throw new Error("Constructor needs object to work with");
  
      this.constructor.properties.forEach(i => {
        if (!Object.keys(props).includes(i))
          throw new Error(`${i} is required`);
      });
    }
  
    introduce() {
      console.log(`Hi, my name is ${this.name}`)
    }
  
    work() {
      console.log("Working...");
    }
  
  }
  
  /*
    We can't do this!
    new Human();
  */
  
  /* ============================== */
  class Police extends Human {
    static properties = [...super.properties, "rank"]
  
    constructor(props) {
      super(props);
      
      this.rank = props.rank;
    }
  
    shoot() {
      console.log("DOR!")
    }
  
    // Override method
    work() {
      this.shoot();
      super.work();
    }
  }
  
  class Densus88 extends Police {
  
    introduce() {
      super.introduce();
      console.log("And I'm a member of Densus88")
    }
  }
  
  const Wiranto = new Police({
    name: "Wiranto",
    gender: "Male",
    rank: "General"
  })
  
  Wiranto.work();
  
  const Nurdin = new Densus88({
    name: "Nurdin",
    gender: "Male",
    rank: "General"
  })
  
  Nurdin.introduce();
  Nurdin.shoot();
  
  /* ============================== */
  
  /* ============================== */
  
  class Doctor extends Human {
    static properties = [...super.properties, "specialization"]
  
    constructor(props) {
      super(props);
      
      this.specialization = props.specialization;
    }
  
    cure(person) {
      console.log(`Cure ${person.name}`);
    }
  
    // Overload method
    work(person) {
      this.cure(person);
      console.log("Doing research...")
  
      super.work();
    }
  }
  
  const Alodoc = new Doctor({
    name: "Alodoc",
    gender: "Male",
    specialization: "Dentist"
  })
  
  Alodoc.work(Wiranto);
  