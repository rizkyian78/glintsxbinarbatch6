/*
  1. Greet another Human
  2. Marry another Human
  3. Introduce themself
  4. Learn new language
  5. If person a tries to marry person b, make sure they've both understand the same language, otherwise, one of them will learn a new languange that both will understand.
*/
// Static = property yang nilainya sama
// To randomly call element inside the array
Object.defineProperty(Array.prototype, 'sample', {
    get: function() {
      return this[
        Math.floor(
          Math.random() * this.length
        )
      ]
    }
  })
  
  Array.prototype.random = function() {
    return this[
      Math.floor(
        Math.random() * this.length
      )
    ]
  }
  
  class Human {
  
    static properties = [
      "name",
      "gender",
      "languages"
    ]
  
    constructor(props) {
      // Validate the props
      this._validate(props);
  
      this.name = props.name;
      this.gender = props.gender;
      this.languages = props.languages;
    }
  
    // Validate, for input inside the constructor
    _validate(props) {
      if (typeof props !== 'object' || Array.isArray(props))
        throw new Error("Constructor needs object to work with");
      
        //To access the static properties = this.constructor
      this.constructor.properties.forEach(i => {
        if (!Object.keys(props).includes(i))
          throw new Error(`${i} is required`);
      });
    }
  
    introduce() {
      console.log(`Hi, my name is ${this.name}`);
    }
  
    greets(person) {
      // Check if the person is a human
      if (!(person instanceof Human))
        throw new Error("You can only greet Human");
      
      console.log(`Hi, ${person.name}!`)
    }
  
    learnLanguage(language) {
      if (typeof language !== "string")
        throw new Error("Languange is must be a string");
  
      if (this.languages.includes(language)) return;
  
      this.languages.push(language);
    }
  
    marry(person) {
      // Check if ready;
      let ready = false;
  
      person.languages.forEach(i => {
        if (this.languages.includes(i))
          return ready = true;
      })
  
      
  
      if (!ready) {
        let fiancees = [this, person];
        let learner = fiancees.sample;
        let matchee = fiancees.indexOf(learner) == 1 ? fiancees[0] : fiancees[1];
  
        learner.learnLanguage(matchee.languages.sample);
      }
  
      this.spouse = person;
      person.spouse = this;
    }
  }
  
  let Fikri = new Human({
    name: "Fikri",
    gender: "Male",
    languages: ["Indonesia", "English"]
  })
  
  let Woman = new Human({
    name: "Woman",
    gender: "Female",
    languages: ["Javanese", "Japanese"]
  })
  
  Fikri.greets(Woman);
  Fikri.learnLanguage("Russian");
  
  Fikri.marry(Woman);
  console.log(Fikri);
  console.log(Woman);
  

//   Getter and Setter onfly