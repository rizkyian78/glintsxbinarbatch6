// Creating an Object
// Factory Function
function createCrircle(radius) {
    return {
        // ES6 feature
        radius,
        draw: () => {
            console.log('draw')
        }
    };
}

const circle = createCrircle(3)

// Constructor Function
function Circle(radius) {
    this.radius = radius;
    this.draw = function() {
        console.log('draw');
    }
}
// to enumerate all the members and object you can use forin loops
let another = new Circle(6);
for(let key in circle) {
    if(typeof circle[key] !== 'function')
    console.log(key, circle[key])
}
// to get the keys of the object
let keys = Object.keys(circle);
console.log(keys);
// Check for the existence of some keys in Object
if('radius' in circle) {
    console.log('Circle Has Radius');
}


/*
 Circle.name = return the name of the function
 Circle.length = return the length
 */

//  Abstraction
function Circle(radius) {
    this.radius = radius;
    let defaultLocation = { x: 0, y: 0};
    let computeOptimumLocation = function(factor) {

    }
    this.draw = function() {
        computeOptimumLocation();
        // defaultLocation
        //this.radius
        console.log('draw');
    }
    Object.defineProperty(this, 'defaultLocation', {
        get: function() {
            return defaultLocation
        },
        set: function(value) {
            if(!value.x || !value.y)
            defaultLocation = value
        }
    })
}
const lingkaran = new Circle(5);
// circle.computeOptimumLocation();
lingkaran.defaultLocation = 1
