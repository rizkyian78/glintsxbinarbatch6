## 4 Pillars Of OOP ##
1. Encapsulation = Reduce Complexity + increase reusability
2. Abstraction = Reduce Complexity + isolate impact changes
3. Inheritance = Eliminate Redundant Code
4. Polymorphism = Refactor Switch Case