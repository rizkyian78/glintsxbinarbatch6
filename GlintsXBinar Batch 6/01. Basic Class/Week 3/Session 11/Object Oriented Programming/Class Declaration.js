/*
* Class Declaration
* Method Signature
* Static and Instance
* Property
* Protoype  = Instance
 */

//  Class Declaration
 class Human {
     constructor(name, address, lang) {
        // Programming Languange in General
        this.name = name;
        this.address = address;
        this.lang = lang
     }
     introduce() {
         console.log(`My name is ${this.name}`);
     }
    //  Call another method inside the class declaration
     dowork() {
        console.log("Waking Up");
        console.log('Taking a bath');
        console.log('OTW Office');
     }
    //  Private
     #goHome = () => {
         console.log('Go Home');
         console.log("Sleep");
     }
    //  Public
     work(){
         this.dowork();
         this.#goHome();
     }
     tame(pet){
         this.pet = Array.isArray(this.pet) ? [...this.pet, pet] : [pet];
        }
     static isLiving = "babi";
     static isMortal = false;
     static destruct = () => {
         console.log(this.isMortal);
     }
 }

 const Ian = new Human("Ian", "Malang", "Indog");
 console.log(Ian.work());
 console.log(Ian.tame("Cats"));
 console.log(Ian);
 console.log(Ian.pet);
 console.log(Ian.work());
 console.log(Ian.isLiving);
 /*
  */
//  Add new prototype on the fly
Ian.prototype

//   Cari Perbedaan function vs arrow function