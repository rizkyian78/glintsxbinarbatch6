// Randomize array of the object
Object.defineProperty(Array.prototype, 'sample', {
    get: function() {
        return this[
            Math.floor(
                Math.random() * this.length
            )
        ]
    }
})

class Human{
    constructor(props) {
        this.name = props.name;
    }
    introduce() {
        console.log(`Hello, my name is ${this.name}`);
    }
    cook() {
        console.log('I can cook');
    }
}
class Chef extends Human{
    constructor(props) {
        super(props);
        this.typeOfCuisine =  props.typeOfCuisine || "I'm just Bluffing";
    };
    #introduce = () => {
        console.log(`Hello, my name is ${this.name} and I'm a ${this.typeOfCuisine.sample} Chef`);
    }
    promote() {
        console.log(`My name is ${this.name} I like women`);
        super.introduce();
        this.#introduce();
    };
    cook(){
        console.log(`I'm better cooking than human`);
    }
};

const Ian = new Chef({
    name: 'Rizky Ian Indiarto',
    typeOfCuisine: ["italian", "French", "Pedesaan"]
})

console.log(Ian.promote());
console.log(Ian.cook());