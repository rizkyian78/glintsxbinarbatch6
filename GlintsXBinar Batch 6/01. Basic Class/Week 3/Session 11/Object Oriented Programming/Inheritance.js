class Human {
    constructor(props) {
        let {name, address, lang} = props;
        this.name = name;
        this.address = address;
        this.lang = lang
    }
    introduce() {
        console.log(`${this.name}`);
    }
    static isLiving = true;
}
class Programmer extends Human {
    constructor(props) {
        super(props);
        console.log(super.constructor.isLiving);
        this.programmingLanguange = props.programmingLanguange || [];
    }
    introduce(){
        console.log('Terbaik');        
    }
}

const ian = new Programmer({
    name: "Ian",
    address: "Malang",
    lang: "jawa",
})

Human.prototype.babi = () => {
    console.log('Binatang');
}

console.log(Human.prototype.babi());