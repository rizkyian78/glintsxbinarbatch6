// Randomize array of the object
Object.defineProperty(Array.prototype, 'sample', {
    get: function() {
        return this[
            Math.floor(
                Math.random() * this.length
            )
        ]
    }
})

class Human {
    constructor(props) {
        let {personAName, personBName,personCName, languangeA, languangeB, languangeC, personAStatus, personBStatus, personCStatus} = props;
        this.personAName = personAName;
        this.personBName = personBName;
        this.personCName = personCName
        this.languangeA = languangeA;
        this.languangeB = languangeB;
        this.languangeC = languangeC;
        this.personAStatus = personAStatus;
        this.personBStatus = personBStatus;
        this.personCStatus = personCStatus;
        this.isMarried = true;
    }
}

class Person extends Human {
    constructor(props) {
        super(props);
    };
    introduceA() {
        console.log(`My Name is ${this.personAName} i'm Unemployed`);
    };
    introduceB(){
        console.log(`My Name is ${this.personBName} i'm Unemployed`);
    }
    introduceC(){
        console.log(`My Name is ${this.personCName} i'm Unemployed`);
    }
    greet(){
        console.log(`Hello, ${this.personAName} nice to meet you, my Name is ${this.personBName}`);
    };
    marry() {
        if(this.personAStatus && this.personBStatus === this.isMarried) {
            return this.languangeA.sample == this.languangeB.sample ? console.log("Officially Married") : console.log(`Please Learn ${this.languangeB.sample} first`)
        } else {
            console.log(`Please choose ${this.personCName}`);
        }
    };
};

const orang = new Person({
    personAName: "Ian",
    personBName: "Mariani",
    personCName: "Khana",
    languangeA: ["Javanese", "Chinese"],
    languangeB: ["English", "Chinese"],
    languangeC: ["Sundanese", "Javanese"],
    personAStatus: true,
    personBStatus: true,
    personCStatus: false
});

orang.marry()
orang.greet()