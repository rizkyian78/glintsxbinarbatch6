// Import Readline Modul
const readline = require('readline');
const calcSquare = require('./square');
// Create Interface
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
const numberRegex = /^[0-9]+$/; // untuk Regular Expression Number

function getAreaInput() {
    rl.question("Side: ", answer => {
        console.log("Here's the Result", calcSquare.calcSquareArea(+answer) )
        rl.close();
    })
}
function getRoundInput() {
    rl.question("Side: ", answer => {
        console.log("Here's the Result", calcSquare.calcSquareRound(+answer))
        rl.close();
    })
};
console.clear();
console.log(`Which Operation Do you want: 
1. Calculate Circle
2. Calculate Triangle
3. Calculate Square
4. Calculate Cube`)
function answerHandler(answer) {
    console.clear();
    switch(+answer) {
        case 1: return getAreaInput();
        case 2: return getRoundInput();
        default:
            console.log("Option Is Not Available")
            rl.close();
    }
};
rl.question("Answer: ", answer => { 
    if(typeof +answer !== 'number') {
        console.log('Think Carefully Before You Type');
        rl.close();
    } else {
        answerHandler(answer);
    }
});

rl.on("close", () => {
    console.log("Well Hello Bois you've Done Well Today");
    process.exit;
})