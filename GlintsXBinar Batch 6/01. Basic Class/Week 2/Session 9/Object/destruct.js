let person = {
  name: "Fikri Rahmat Nurhidayat",
  address: "Solo"
}

// The old way
// let name = person.name;
// let address = person.address;

// Declare object destruction
let { name: iniNamaVariableBaru, address } = person;

console.log(iniNamaVariableBaru); // Fikri
console.log(address);
