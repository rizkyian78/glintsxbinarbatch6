# Session-06

## Array

### Looping in Array

There are several ways:

* for
* for each
* for of

```js
// Using for
// i = 0 -> initial value
// i < 10 -> break statement
// i++ -> incremental
for(let i =0; i < 10; i++){
    // Do something here
    console.log('Hello World');
}

// Using for each
const numbers = [1,2,3];
numbers.forEach(number => console.log(number));

// Using for of
for (n of numbers){
    console.log(n);
}
```

### Methods in Array
* push
```js
// This is used to add new items to the end of an array, and returns the new length.
```
* pop
```js
// This is used to removes the last element of an array, and returns that element.
```
* shift
```js
// This is used to removes the first item of an array.
```
* unshift
```js
// This is used to adds new items to the beginning of an array, and returns the new length.
```
For the details, you can visit this <a href="https://www.w3schools.com/jsref/jsref_obj_array.asp"> web</a>
## Object

### Looping in Object
```js
// Using for in
const Student = {
    name: 'rijal',
    age: 25
}
for (s in Student){
    console.log(s);
    /*
    It will print its keys, like this:
    name
    age
    */
}
```

## Logical Operator

* &&

Returns true if both operands are truthy and false otherwise

* ||

If any of its arguments are true, it returns true, otherwise it returns false.

```js
let a = 10;
let b = 15;
if (a > 5 && b > 5){
    // Because, these 2 conditionals are true, so this following code will run
    console.log('This is an AND Operator');
}else{

}
if (a > 5 || b < 10) {
    // Because one of 1 arguments is true, then it will return true
    console.log('This is an OR Operator');
}
```
