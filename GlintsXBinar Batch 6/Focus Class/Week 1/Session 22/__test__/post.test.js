const request = require('supertest');
const app = require('../index');
const { post } = require('../models');

describe('Posts API Collection', () => {
  
  beforeAll(() => {
    return post.destroy({
      truncate: true
    })
  })

  afterAll(() => {
    return post.destroy({
      truncate: true
    })
  })

  describe('GET /api/v1/posts/', () => {
    test('Status code 200 should successfully get all data posts', (done) => {
        request(app)
            .get('/api/v1/posts/')
            .set('Content-Type', 'application/json')
            .then((res) => {
                expect(res.statusCode).toBe(200)
                expect(res.body.status).toEqual('Success')
                expect(res.body).toHaveProperty('posts')
                done()
            })
    })
    test('Status code 422 should don`t get all data posts', (done) => {
        request(app)
            .get('/api/v1/posts/')
            .set('Content-Type', 'application/json')
            .then((res) => {
                expect(res.statusCode).toBe(404)
                expect(res.body.status).toEqual('Fail')
                expect(res.body).toHaveProperty('error')
                done()
            })
    })
})

  describe('POST /api/v1/posts', () => {

    test('Should succesfully create new post', done => {
      request(app)
        .post('/api/v1/posts')
        .set('Content-Type', 'application/json')
        .send({ title: 'Hello World', body: 'Lorem Ipsum' })
        .then(res => {
          expect(res.statusCode).toEqual(201);
          expect(res.body.status).toEqual('Success');

        //   expect(res.body.data.post)
        //     .toEqual(
        //       expect.objectContaining({
        //         id: expect.any(Number),
        //         title: expect.any(String),
        //         body: expect.any(String),
        //       })
        //     );
          done();
        })
    })  
    test('Should not create new post', done => {
      request(app)
        .post('/api/v1/posts')
        .set('Content-Type', 'application/json')
        .send({ body: 'mbutmbut' })
        .then(res => {
          expect(res.statusCode).toEqual(422);
          expect(res.body.status).toEqual('Fail');
          expect(res.body).toHaveProperty('error')
          done();
        })
    })
  })

  describe('PUT /api/v1/posts/:id',() => {
    test('should successfully update the post', done => {
      request(app)
        .put('/api/v1/posts/1')
        .set('Content-Type', 'application/json')
        .send({title: 'Hello mbut', body: 'lorem'})
        .then(res => {
          expect(res.statusCode).toEqual(202)
          expect(res.body.status).toEqual('Success')
          done();
        })
    })
    test('should not update the post', done => {
      request(app)
      .put('/api/v1/posts/1')
      .set('Content-Type', 'application/json')
      .send({body: null, title: null})
      .then(res => {
        expect(res.statusCode).toEqual(422);
        expect(res.body.status).toEqual('Fail');
        done();
      })
    })
  })

  describe('DELETE /api/v1/posts/:id',() => {
    test('should successfully DELETE the post', done => {
      request(app)
        .delete('/api/v1/posts/1')
        .set('Contentl-Type', 'application/json')
        .then(res => {
          expect(res.statusCode).toEqual(200)
          expect(res.body.status).toEqual('Success')
          done();
        })
    })
    test('should not delete the post', done => {
      request(app)
      .delete('/api/v1/posts/h')
      .set('Content-Type', 'application/json')
      .then(res => {
        expect(res.statusCode).toEqual(422);
        expect(res.body.status).toEqual('Fail');
        done();
      })
    })
  })    
})
