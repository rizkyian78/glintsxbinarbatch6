const {post} = require('../models');

class Test {
    static get(req, res) {
        res.status(200).json({
            status: "Hello World"
        })
    }
    static async getAll(req, res) {
        try {
            const data = await post.findAll();
            res.status(200).json({
                status: 'Success',
                data
            })
        } catch (err) {
            res.status(422).json({
                status: 'Fail',
                error: [err.message]
            })            
        }
    }
    static async create(req, res) {
        try {
        const data = await post.create(req.body);
        res.status(201).json({
            status: "Success",
            data
        })
    } catch(err) {
        res.status(422).json({
            status: "Fail",
            error: [err.message]
        })
    }
    }
    static async update(req, res) {
        try {
        await post.update(req.body, {
            where: {id: req.params.id}
        })
        res.status(202).json({
            status: "Success",
            message: `ID: ${req.params.id} has been updated`
            })
        } catch(err) {
            res.status(422).json({
                status: "Fail",
                error: [err.message]
            })
        }
    }
    static async delete(req, res) {
        try {
            await post.destroy({where: {id: req.params.id}});
            res.status(200).json({
                status: "Success",
                message: `${req.params.id} has been deleted`
            })
        } catch (err) {
            res.status(422).json({
                status: "Fail",
                message: [err.message]
            })
        }
    }
}

module.exports = Test