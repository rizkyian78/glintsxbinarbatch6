function countSwaps(a) {
    let swapCount = 0;
    for(let i = 0; i < a.length; i++) {
        for(let j = i + 1; j < a.length; j++) {
            if(a[i] > a[j]) {
                let temp = a[i];
                a[i] = a[j];
                a[j] = temp;
                swapCount++
            }
        }    
    }
    return console.log(`Array is sorted in ${swapCount} swaps.
    First Element: ${a[0]}
    Last Element: ${a[a.length - 1]}`)
}

countSwaps([3, 2, 1])