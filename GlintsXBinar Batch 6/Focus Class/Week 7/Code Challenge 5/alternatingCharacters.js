function alternatingCharacters(s) {
    let last = s.charAt(0);
    let counted = 0;
    for(let i = 1; i < s.length; i++) {
        if(last === s.charAt(i)) {
            counted++;
        } else {
            last = s.charAt(i);
        }
    }
    return counted;
}

console.log(alternatingCharacters('AAABBB'))